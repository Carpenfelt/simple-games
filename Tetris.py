# coding=utf-8

import PySimpleGUI as sg
import numpy as np
import copy

SHOW_GAME_IN_TERMINAL = False

W, H = 10,18
BOX_SIZE = 20
SMALL_BOX_SIZE = 5
CHAR_W, CHAR_H = 4,5
SCORE_W, SCORE_H = 1,4
N_CHARS = 10

COLOR_CODES = [
    'white',
    'brown',
    'cyan',
    'yellow',
    'red',
    'blue',
    'green',
    'purple',
    'pink'
]
HEX_COLORS = [
    ('#ffffff','#ffffff'),
    ('#360407','#360407'),
    ('#ff0000','#ff0000'),
    ('#ff6600','#ff6600'),
    ('#00f2ff','#ffffff'),
    ('#1100ff','#ffffff'),
    ('#aa00ff','#ffffff'),
    ('#ffffff','#ffffff'),
    ('#ffffff','#ffffff'),
]
SHAPES = [
    [(0,-1),(0,0),(0,1),(0,2)], #Pinnen
    [(0,0),(0,1),(1,0),(1,1)], #Kvadrat
    [(0,0),(1,0),(-1,0),(0,1)], #Triangeln
    [(0,1),(0,0),(0,-1),(1,-1)], #L
    [(0,1),(0,0),(0,-1),(-1,-1)], #Omvänt L
    [(0,1),(0,0),(-1,0),(-1,-1)], #S  
    [(0,1),(0,0),(1,0),(1,-1)], #Omvänt S   
]
SHAPE_CENTERS = [
    (2,1.5), #Pinnen
    (1.5,1.5), #Kvadrat
    (2,1.5), #Triangeln
    (1.5,2), #L
    (2.5,2), #Omvänt L
    (2.5,2), #S
    (1.5,2), #Omvänt S   
]
NUMBERS = [
    '###'
    '# #'
    '# #'
    '# #'
    '###',

    ' # '
    ' # '
    ' # '
    ' # '
    ' # ',

    '###'
    '  #'
    '###'
    '#  '
    '###',

    '###'
    '  #'
    ' ##'
    '  #'
    '###',

    '# #'
    '# #'
    '###'
    '  #'
    '  #',

    '###'
    '#  '
    '###'
    '  #'
    '###',

    '###'
    '#  '
    '###'
    '# #'
    '###',

    '###'
    '  #'
    '  #'
    '  #'
    '  #',

    '###'
    '# #'
    '###'
    '# #'
    '###',

    '###'
    '# #'
    '###'
    '  #'
    '###',
]

def char_generator(n,X,Y):
    for x in range(X):
         for y in range(Y):
            if NUMBERS[n][y*X+x] == "#":
                yield (x,Y-y-1)
            else:
                continue
LETTERS = {
    'S' : [(0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(0,4),(1,4),(2,4)],
    'C': [(0,0),(1,0),(2,0),(0,1),(0,2),(0,3),(0,4),(1,4),(2,4)],
    'O': [(0,0),(1,0),(2,0),(0,1),(0,2),(0,3),(0,4),(1,4),(2,4),(2,1),(2,2),(2,3)],
    'R' : [(0,0),(2,0),(2,1),(1,2),(0,2),(0,3),(0,4),(1,4),(2,4),(0,1),(2,3)],
    'E': [(0,0),(1,0),(2,0),(0,1),(0,2),(0,3),(0,4),(1,4),(2,4),(1,2),(2,4),],
    ':' : [(1,1),(1,3)],
    '0' : [c for c in char_generator(0,3,5)],
    '1' : [c for c in char_generator(1,3,5)],
    '2' : [c for c in char_generator(2,3,5)],
    '3' : [c for c in char_generator(3,3,5)],
    '4' : [c for c in char_generator(4,3,5)],
    '5' : [c for c in char_generator(5,3,5)],
    '6' : [c for c in char_generator(6,3,5)],
    '7' : [c for c in char_generator(7,3,5)],
    '8' : [c for c in char_generator(8,3,5)],
    '9' : [c for c in char_generator(9,3,5)],
}

SCORES = [
    0,
    W, # 1 Row
    W*3, # 2 Rows
    W*6, # 3 Rows
    W*10, # 4 Rows
]
FONT = ('Helvetica_bold', 30) #Score font

GAME_SPEED = 10 #Milliseconds per game "tick"

def out_of_bounds(x,y): # Check wheter a coordinate is within the game frame
    return not (-1<x<W and -1<y<H)

def main_menu():
    layout = [
        [sg.Button('Continue'),sg.Button('Play Again'), sg.Button('Exit')]
    ]
    window = sg.Window('Menu', layout)
    event, values = window.read()
    window.close()
    return event


class Tetris:

    class Block:
        
        def __init__(self,block_type = -1):
            self.x, self.y = 1+np.random.randint(W-2), H-1
            self.block_type = np.random.randint(len(SHAPES)) if block_type == -1 else block_type
            self.shape = SHAPES[self.block_type]
            self.color = COLOR_CODES[2+self.block_type]

        def fall(self):
            self.y -=1

        def move_left(self):
            self.x-=1
        
        def move_right(self):
            self.x+=1

        def rotate_clockwise(self):
            if self.block_type == 1: return #Dont rotate the square
            self.shape = [(-y,x) for x,y in self.shape]

        def rotate_anti_clockwise(self):
            if self.block_type == 1: return #Dont rotate the square
            self.shape = [(y,-x) for x,y in self.shape]

        def get_coords(self,y = None):
            if y == None: y = self.y
            return [(self.x+dx, y+dy) for dx, dy in self.shape]

    def __init__(self):
        self.score = 0
        self.speed = GAME_SPEED
        self.generate_grid()
        self.blocks = []
        self.initialize_graphics()
        self.next_block = self.Block()
        self.new_block()
        self.draw_score_str()
        self.play()

    def generate_grid(self):
        self.grid = [[COLOR_CODES[0] for x in range(W)] for y in range(H)]
        #self.grid = [[COLOR_CODES[0] if (x > W - 2 or y>4) else COLOR_CODES[1] for x in range(W)] for y in range(H)]

    def initialize_graphics(self):
        self.graph = sg.Graph(
            canvas_size=(W*BOX_SIZE*2, H*BOX_SIZE*2),
            graph_bottom_left=(0, 0),
            graph_top_right=(W*BOX_SIZE, H*BOX_SIZE),
            key='-GRAPH-',
            change_submits=False,
            drag_submits=False,
            background_color='lightblue'
        )
        self.preview_graph = sg.Graph(
            canvas_size=(SMALL_BOX_SIZE * 16, SMALL_BOX_SIZE * 20),
            graph_bottom_left=(2,0),
            graph_top_right=(SMALL_BOX_SIZE * 5 - 2, SMALL_BOX_SIZE * 5),
            key='-PREVIEW_GRAPH-',
            change_submits=False,
            drag_submits=False,
            background_color='lightblue'
        )
        self.score_graph = sg.Graph(
            canvas_size=( (SMALL_BOX_SIZE * (CHAR_W * N_CHARS+2)) *SCORE_W, SMALL_BOX_SIZE * (CHAR_H+1) * SCORE_H),
            graph_bottom_left=(-SMALL_BOX_SIZE, -SMALL_BOX_SIZE),
            graph_top_right=(SMALL_BOX_SIZE * (CHAR_W * N_CHARS), SMALL_BOX_SIZE * (CHAR_H+1)),
            key='-SCORE_GRAPH-',
            change_submits=False,
            drag_submits=False,
            #background_color='lightblue'
        )
        layout = [
            [sg.Column([[self.score_graph]], element_justification='left', expand_x=True), sg.Column([[self.preview_graph]], justification='right')],
            [self.graph]
        ]
        self.window = sg.Window(
            'TETRIS',
            layout, 
            finalize=True,
            return_keyboard_events=True,
            use_default_focus=False
        )
        
    
    def draw_score_str(self):
        self.score_graph.erase()
        xx = 0
        for index, letter in enumerate(self.score_str()):
            if letter  == " ":
                xx+=3
                continue
            for dx,y in LETTERS[letter]:
                x = xx+dx

                self.score_graph.draw_rectangle(
                        (x* SMALL_BOX_SIZE,y* SMALL_BOX_SIZE),
                        ((x+1) * SMALL_BOX_SIZE, (y+1) * SMALL_BOX_SIZE),
                        line_color='black',
                        fill_color=COLOR_CODES[(2+index)%len(COLOR_CODES)]
                    )
            xx += CHAR_W
        
    def score_str(self):
        #return 'SCORE:0123456789'
        return f'SCORE:{self.score}'
    
    def draw_graphics(self):
        self.graph.erase()
        grid = self.generate_grid_with_block()
        for y in range(H):
            for x in range(W):
                color = grid[y][x]
                if color == 'white': continue
                self.graph.draw_rectangle(
                    (x * BOX_SIZE, y * BOX_SIZE),
                    ((x+1) * BOX_SIZE, (y+1) * BOX_SIZE),
                    line_color='black',
                    fill_color=color
                )
    
    def draw_preview(self):
        self.preview_graph.erase()
        self.next_block = self.Block()
        xx,yy = SHAPE_CENTERS[self.next_block.block_type]
        for dx,dy in self.next_block.shape:
            x,y = dx+xx, dy+yy
            self.preview_graph.draw_rectangle(
                    (x* SMALL_BOX_SIZE,y* SMALL_BOX_SIZE),
                    ((x+1) * SMALL_BOX_SIZE, (y+1) * SMALL_BOX_SIZE),
                    line_color='black',
                    fill_color=self.next_block.color
                )

        
    def play(self):
        GAME_OVER = False
        self.GAME_TIME = 0 
        while not GAME_OVER:
            self.draw_graphics()
            event, values = self.window.read(timeout = self.speed)
            self.handle_event(event,values)
            if event == sg.WIN_CLOSED:
                self.window.close()
                break

    def update_game(self):
        if self.collision_check():
            self.generate_grid_with_block(merge=True)
            self.new_block()
            self.check_for_full_rows()
        self.current_block.fall()
        self.print_grid()

    def handle_event(self,event,values):
        if event == 'Escape:27':
            res = main_menu()
            if res == 'Exit':
                self.window.close()
        if event == '__TIMEOUT__':
            self.GAME_TIME += 1
            if self.GAME_TIME >= 50:
                self.update_game()
                self.GAME_TIME = 0
        else:
            #print(values)
            #print(event)
            pass
        if event == ' ':
            while True:
                self.current_block.fall()
                if self.collision_check():
                    self.generate_grid_with_block(merge=True)
                    self.new_block()
                    self.check_for_full_rows()
                    break
        elif event == 'Down:40' or event == 's':
            self.update_game()
        elif event == 'Left:37' or event == 'a':
            self.current_block.move_left()
            if not self.is_legal_block_position():
                self.current_block.move_right()
        elif event == 'Right:39' or event == 'd':
            self.current_block.move_right()
            if not self.is_legal_block_position():
                self.current_block.move_left()
        elif event == 'q':
            self.current_block.rotate_anti_clockwise()
            if not self.is_legal_block_position():
                self.current_block.rotate_clockwise()
        elif event == 'e' or event == 'Up:38':
            self.current_block.rotate_clockwise()
            if not self.is_legal_block_position():
                self.current_block.rotate_anti_clockwise()
        
            
    def generate_grid_with_block(self, merge = False, shadow = False):
        if not merge:
            grid = copy.deepcopy(self.grid)
            for yy in range(H):
                coords = self.current_block.get_coords(yy)
                if self.is_legal_block_position(coords=coords):
                    #print(yy,coords)
                    for x,y in coords:
                        grid[y][x] = 'grey'
                    break
        for x,y in self.current_block.get_coords():
            if out_of_bounds(x,y):continue
            if merge:
                self.grid[y][x] = COLOR_CODES[1]
            else:
                grid[y][x] = self.current_block.color 
        if merge: return
        return grid

    def collision_check(self):
        for x,y in self.current_block.get_coords():
            if out_of_bounds(x,y):continue
            if y == 0:
                print("FLOOR COLLISION!")
                return True
            if self.grid[y-1][x] == COLOR_CODES[1]:
                print("COLLISION!")
                return True
        return False

    def check_for_full_rows(self):
        n_full_rows= 0
        row_indices_to_delete = []
        for y, row in enumerate(self.grid):
            is_full_row = True
            for cell in row:
                if not cell == COLOR_CODES[1]:
                    is_full_row = False
                    break
            if is_full_row:
                n_full_rows += 1
                row_indices_to_delete.append(y)
        self.score += SCORES[n_full_rows]
        self.draw_score_str()
        if len(row_indices_to_delete)>0:
            for y in row_indices_to_delete[::-1]:
                del self.grid[y]
            for y in row_indices_to_delete:
                self.grid.append([COLOR_CODES[0] for _ in range(W)])

    def print_grid(self):
        if not SHOW_GAME_IN_TERMINAL: return
        grid = self.generate_grid_with_block()
        text='___TETRIS___\n'
        for row in grid[::-1]:
            text+='|'
            for cell in row:
                text+=' ' if cell == COLOR_CODES[0] else '#'
            text+='|\n'
        print(text)


    def is_legal_block_position(self,block = None, coords = None):
        if block == None: block = self.current_block
        if coords == None: coords = block.get_coords()
        for x,y in coords:
            if y>=H:continue
            if out_of_bounds(x,y):
                return False
            if self.grid[y][x] == COLOR_CODES[1]:
                return False
        return True

    def new_block(self):
        self.current_block = self.next_block
        self.next_block = self.Block()
        self.draw_preview()

if (__name__ == "__main__"):
    game = Tetris()
